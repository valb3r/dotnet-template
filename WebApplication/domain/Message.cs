using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace WebApplication.domain
{
    public class Message
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Body { get; set; }
    }

    public class BookMap : ClassMapping<Message>
    {
        public BookMap()
        {
            Id(x => x.Id, map => map.Generator(Generators.Native));
            Property(x => x.Title);
            Property(x => x.Body);
        }
    }
}