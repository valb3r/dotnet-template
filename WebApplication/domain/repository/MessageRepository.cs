using NHibernate;

namespace WebApplication.domain.repository
{
    public class MessageRepository : Repository<Message>
    {
        public MessageRepository(ISession session) : base(session)
        {
        }
    }
}