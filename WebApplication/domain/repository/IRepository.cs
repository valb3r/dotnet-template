using System.Collections.Generic;
using NHibernate.Criterion;

namespace WebApplication.domain.repository
{
    public interface IRepository<T>
    {
        int Add(T entity);
        T GetById(int id);
    }
}