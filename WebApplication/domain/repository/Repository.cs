using NHibernate;
using WebApplication.config;

namespace WebApplication.domain.repository
{
    public class Repository<T> : IRepository<T>
    {
        readonly ISession _activeSession;

        public Repository(ISession activeSession)
        {
            _activeSession = activeSession;
        }
        
        public int Add(T entity)
        {
            int newId = (int)_activeSession.Save(entity);
            _activeSession.Flush();
            return newId;
        }

        public T GetById(int id)
        {
            return _activeSession.Get<T>(id);
        }
    }
}