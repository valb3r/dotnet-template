using System;
using System.Collections.Generic;
using System.Reflection;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Dialect;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using WebApplication.domain;

namespace WebApplication.config
{
    public class NHibernateHelper
    {
        public static ISession OpenSession() {
            var cfg = new Configuration()
                .DataBaseIntegration(db => {
                    db.ConnectionString = @"Server=postgres;Port=5432;Database=net.demo;User Id=postgres;Password=docker;";
                    db.Dialect<PostgreSQLDialect>();
                });
            var mapper = new ModelMapper();
            mapper.AddMappings(Assembly.GetExecutingAssembly().GetExportedTypes());
            HbmMapping mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
            cfg.AddMapping(mapping);
            new SchemaUpdate(cfg).Execute(true, true);
            ISessionFactory sessionFactory = cfg.BuildSessionFactory();
            return sessionFactory.OpenSession();
        }
    }
}