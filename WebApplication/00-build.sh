#!/bin/bash

rm -rf publish
dotnet build || exit 1
dotnet publish -c Release -o publish || exit 1

cd publish || exit 1
zip -r ../function.zip *  || exit 1