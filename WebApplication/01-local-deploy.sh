#!/bin/bash
set -x

# Ensure local stack is up
while (curl -v --silent http://localhost:4566/health 2>&1 || echo "starting") | grep "starting"; do sleep 1; echo "waiting for localstack..."; done

AWS="aws --endpoint-url=http://localhost:4566 --region us-east-1 --no-cli-pager"
VERSION="2021-09-20"

export AWS_ACCESS_KEY_ID=test
export AWS_SECRET_ACCESS_KEY=test

$AWS iam create-role --role-name lambda-dotnet-ex --assume-role-policy-document '{"Version": "'$VERSION'", "Statement": [{ "Effect": "Allow", "Principal": {"Service": "lambda.amazonaws.com"}, "Action": "sts:AssumeRole"}]}'
$AWS iam attach-role-policy --role-name lambda-dotnet-ex --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
$AWS lambda create-function --function-name lambda-dotnet-function --zip-file fileb://function.zip --handler WebApplication::WebApplication.LambdaFunction::FunctionHandlerAsync --runtime dotnetcore3.1 --role arn:aws:iam::000000000000:role/lambda-dotnet-ex

API_ID=$($AWS apigateway create-rest-api --name 'API Test' | jq -r ".id")
PARENT_ID=$($AWS apigateway get-resources --rest-api-id "$API_ID"  | jq -r ".items[0].id")
RESOURCE_ID=$($AWS apigateway create-resource --rest-api-id "$API_ID" --parent-id "$PARENT_ID" --path-part "{proxy+}" | jq -r ".id")
$AWS apigateway put-method --rest-api-id "$API_ID" --resource-id "$RESOURCE_ID" --http-method ANY --request-parameters "method.request.path.id=true" --authorization-type "NONE"
$AWS apigateway put-integration --rest-api-id "$API_ID" --resource-id "$RESOURCE_ID" --http-method ANY --type AWS_PROXY --integration-http-method POST --uri "arn:aws:apigateway:us-east-1:lambda:path/$VERSION/functions/arn:aws:lambda:us-east-1:000000000000:function:lambda-dotnet-function/invocations" --passthrough-behavior WHEN_NO_MATCH
$AWS apigateway create-deployment --rest-api-id "$API_ID" --stage-name test

echo "API ROOT is http://localhost:4566/restapis/"$API_ID"/test/_user_request_/"
curl http://localhost:4566/restapis/"$API_ID"/test/_user_request_/hello/user
