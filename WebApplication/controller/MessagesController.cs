using System;
using System.Collections.Generic;
using Amazon.Lambda.Core;
using Microsoft.AspNetCore.Mvc;
using WebApplication.domain;
using WebApplication.domain.repository;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]
namespace WebApplication.controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private readonly IRepository<Message> _messages;

        public MessagesController(IRepository<Message> messages)
        {
            _messages = messages;
        }
        
        // GET /api/Messages/hello/user
        [HttpGet("/hello/user")]
        public IActionResult GetHello()
        {
            return new JsonResult(new Dictionary<string, string> {["message"] = $"Hello, time is {DateTime.Now}"});
        }

        // GET /api/Messages/3
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var message = _messages.GetById(id);
            if (null == message)
            {
                return StatusCode(404);
            }

            return new JsonResult(new Dictionary<string, string> {["id"] = id.ToString(), ["title"] = message.Title, ["body"] = message.Body});
        }
        
        // POST /api/Messages
        [HttpPost]
        public IActionResult Post(Message message)
        {
            var id = _messages.Add(message);
            return new JsonResult(new Dictionary<string, string>() {["id"] = id.ToString(), ["title"] = message.Title});
        }
    }
}